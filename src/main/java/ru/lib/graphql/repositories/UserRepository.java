package ru.lib.graphql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.lib.graphql.domain.User;


@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

  @Query("select u from User u where u.email = :email")
  User findByEmail(@Param("email") final String email);

  @Query("select u from User u where u.token = :token")
  User findByToken(@Param("token") final String token);
}
