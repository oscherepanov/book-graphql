package ru.lib.graphql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.lib.graphql.domain.Reader;

@Repository
public interface ReaderRepository extends JpaRepository<Reader, Integer> {
}
