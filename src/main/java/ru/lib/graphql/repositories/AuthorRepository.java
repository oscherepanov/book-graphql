package ru.lib.graphql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.lib.graphql.domain.Author;

import java.util.List;
import java.util.Set;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {
  @Query("select a from Author a where a.id in :ids")
  List<Author> findByIds(@Param("ids") final List<Integer> ids);
}
