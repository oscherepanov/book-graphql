package ru.lib.graphql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.lib.graphql.domain.Journal;

@Repository
public interface JournalRepository extends JpaRepository<Journal, Integer> {
}
