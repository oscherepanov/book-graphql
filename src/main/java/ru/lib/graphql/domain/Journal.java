package ru.lib.graphql.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
@Table(name = "journals")
public class Journal {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  @Enumerated(EnumType.ORDINAL)
  private Action action;

  @ManyToOne(optional = false)
  @JoinColumn(name = "employee_id")
  private Employee employee;

  @ManyToOne(optional = false)
  @JoinColumn(name = "reader_id")
  private Reader reader;

  @ManyToOne(optional = false)
  @JoinColumn(name = "book_id")
  private Book book;

  private String created;
}
