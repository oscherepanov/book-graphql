package ru.lib.graphql.domain;

public enum Action {
  GIVE_OUT, PICK_UP
}
