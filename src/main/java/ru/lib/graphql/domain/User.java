package ru.lib.graphql.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Data
@Entity
@NoArgsConstructor
@Table(name = "users")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private Integer id;

  @NonNull
  @Column(unique = true, nullable = false)
  private String name;

  @NonNull
  @Email
  @Column(unique = true, nullable = false)
  private String email;

  @NonNull
  @Column(nullable = false)
  private String password;

  @Column(unique = true)
  private String token;
}
