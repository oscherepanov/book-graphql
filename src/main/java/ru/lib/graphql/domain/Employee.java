package ru.lib.graphql.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@Table(name = "employees")
public class Employee extends Person {
  @ManyToOne(optional = false)
  @JoinColumn(name = "position_id")
  private Position position;

  @OneToMany(fetch = FetchType.EAGER)
  @JoinColumn(name = "employee_id")
  private List<Journal> journals;
}
