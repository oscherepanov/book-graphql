package ru.lib.graphql.domain;

import graphql.schema.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class GraphQLLocalDateTime extends GraphQLScalarType {

  public GraphQLLocalDateTime() {
    super("LocalDateTime", "Local date time", new Coercing<LocalDateTime, String>() {

      @Override
      public String serialize(Object o) throws CoercingSerializeException {
        if (o instanceof LocalDateTime)
          return Timestamp.valueOf((LocalDateTime) o).toString();
        return null;
      }

      @Override
      public LocalDateTime parseValue(Object o) throws CoercingParseValueException {
        if (o instanceof String)
          return LocalDateTime.parse((String)o);
        return null;
      }

      @Override
      public LocalDateTime parseLiteral(Object o) throws CoercingParseLiteralException {
        return null;
      }
    });
  }
}
