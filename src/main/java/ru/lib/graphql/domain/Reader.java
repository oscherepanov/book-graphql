package ru.lib.graphql.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "readers")
public class Reader extends Person {
  @OneToMany(fetch = FetchType.EAGER)
  @JoinColumn(name = "reader_id")
  private List<Journal> journals;
}
