package ru.lib.graphql.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Set;

@Entity
@NoArgsConstructor
@Data
@Table(name = "books")
public class Book {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Integer id;

  @Column(nullable = false)
  private String name;

  private int year;

  @ManyToOne(optional = false)
  @JoinColumn(name = "genre_id")
  private Genre genre;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(name = "author_books",
    joinColumns = @JoinColumn(name="book_id"),
    inverseJoinColumns = @JoinColumn(name="author_id"))
  @JsonManagedReference
  Set<Author> authors;

  @OneToMany(fetch = FetchType.EAGER)
  @JoinColumn(name = "book_id")
  private List<Journal> journals;

}
