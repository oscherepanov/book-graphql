package ru.lib.graphql.graphql.aspects;

import graphql.schema.DataFetchingEnvironment;
import graphql.servlet.GraphQLContext;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.User;
import ru.lib.graphql.errors.UnauthenticatedAccessException;
import ru.lib.graphql.repositories.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;


@Aspect
@Component
public class SignedAspect {

  @Autowired
  private UserRepository userRepository;

  @Before("@annotation(ru.lib.graphql.graphql.annotation.Signed)")
  public void doASecurityCheck(JoinPoint joinPoint) {
    DataFetchingEnvironment env = getDataFetchingEnvironment(joinPoint.getArgs());
    if (env == null  || !isAuthorized(env)) {
      throw new UnauthenticatedAccessException("Sorry, you do not have enough rights to do that!");
    }
  }

  private DataFetchingEnvironment getDataFetchingEnvironment(Object[] args){
    for (Object arg : args) {
      Class clazz = arg.getClass();
      if (clazz.getName().contains ("graphql.schema.DataFetchingEnvironment"))
        return (DataFetchingEnvironment) arg;
    }
    return null;
  }

    private boolean isAuthorized(DataFetchingEnvironment env) {
      GraphQLContext context = env.getContext();
      HttpServletRequest httpRequest = context.getHttpServletRequest().get();
      Enumeration<String> enumerateToken = httpRequest.getHeaders("access-token");
      if (enumerateToken.hasMoreElements()){
        String token = enumerateToken.nextElement();
        User user = userRepository.findByToken(token);
        return user != null;
      }
      return false;
  }
}
