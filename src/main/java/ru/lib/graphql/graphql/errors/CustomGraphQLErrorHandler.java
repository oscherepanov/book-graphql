package ru.lib.graphql.graphql.errors;

import graphql.ErrorType;
import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.language.SourceLocation;
import graphql.servlet.GraphQLErrorHandler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomGraphQLErrorHandler implements GraphQLErrorHandler {
  @Override
  public List<GraphQLError> processErrors(List<GraphQLError> list) {
    return list.stream().map(e -> getNested(e)).collect(Collectors.toList());
  }

  private GraphQLError getNested(GraphQLError e) {
     if (e instanceof ExceptionWhileDataFetching){
       ExceptionWhileDataFetching dataFetching = (ExceptionWhileDataFetching) e;
       if (dataFetching.getException() instanceof GraphQLError){
         return (GraphQLError) dataFetching.getException();
       }
       else {
        Throwable err = dataFetching.getException();
        return new GraphQLError() {
          @Override
          public String getMessage() {
            return err.getMessage();
          }

          @Override
          public List<SourceLocation> getLocations() {
            return null;
          }

          @Override
          public ErrorType getErrorType() {
            return ErrorType.ValidationError;
          }
        };
       }
     }
     return e;
  }
}
