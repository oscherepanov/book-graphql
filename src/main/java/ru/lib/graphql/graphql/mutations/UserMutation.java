package ru.lib.graphql.graphql.mutations;

import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.User;
import ru.lib.graphql.services.UserService;
import ru.lib.graphql.services.requests.AuthData;

@Component
public class UserMutation extends Mutation {
  private final UserService userService;

  public UserMutation(UserService userService) {
    this.userService = userService;
  }

  public User createUser(String name, AuthData auth){
    return userService.createUser(name, auth);
  }
}
