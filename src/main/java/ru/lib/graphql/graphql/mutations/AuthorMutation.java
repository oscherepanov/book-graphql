package ru.lib.graphql.graphql.mutations;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.Author;
import ru.lib.graphql.services.AuthorService;
import ru.lib.graphql.services.requests.AuthorCreateRequest;
import ru.lib.graphql.services.requests.AuthorEditRequest;

@Component
public class AuthorMutation extends Mutation {
  @Autowired
  private AuthorService authorService;

  public Author createAuthor(AuthorCreateRequest request) {
    return authorService.createAuthor(request);
  }

  public Author updateAuthor(AuthorEditRequest request) throws NotFoundException {
    return authorService.updateAuthor(request);
  }

  public Boolean deleteAuthor(Integer id) {
    return authorService.deleteAuthor(id);
  }
}
