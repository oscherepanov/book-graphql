package ru.lib.graphql.graphql.mutations;

import graphql.schema.DataFetchingEnvironment;
import graphql.servlet.GraphQLContext;
import org.apache.catalina.core.ApplicationPart;
import org.springframework.stereotype.Component;
import ru.lib.graphql.services.UploadFileService;

import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Component
public class UploadFileMutation extends Mutation {

  private final UploadFileService service;

  public UploadFileMutation(UploadFileService service) {
    this.service = service;
  }

  public String uploadFiles(DataFetchingEnvironment env){
    GraphQLContext context =  env.getContext();
    Optional<Map<String, List<Part>>> optionalParts = context.getFiles();
    if (optionalParts.isPresent()){
      Map<String, List<Part>> parts = optionalParts.get();
      if (!parts.containsKey("file"))
        return "";
      List<Part> values = parts.get("file");
      List<InputStream> streams = new ArrayList<>();
      try {
        for (Part part: values) {
          streams.add(part.getInputStream());
        }
      }
      catch (IOException e)
      {
        // TODO add exception
      }
      String key = parts.get("file").get(0).getSubmittedFileName();
      return service.uploadFile(key, streams);
    }
    return "";
  }
}
