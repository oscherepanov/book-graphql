package ru.lib.graphql.graphql.mutations;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.Employee;
import ru.lib.graphql.services.EmployeeService;
import ru.lib.graphql.services.requests.EmployeeCreateRequest;
import ru.lib.graphql.services.requests.EmployeeEditRequest;

@Component
public class EmployeeMutation extends Mutation {

  @Autowired
  private EmployeeService employeeService;

  public Employee createEmployee(EmployeeCreateRequest request) throws NotFoundException {
    return employeeService.createEmployee(request);
  }

  public Employee updateEmployee(EmployeeEditRequest request) throws NotFoundException {
    return employeeService.updateEmployee(request);
  }

  public Boolean deleteEmployee(Integer id) {
    return employeeService.deleteEmployee(id);
  }
}
