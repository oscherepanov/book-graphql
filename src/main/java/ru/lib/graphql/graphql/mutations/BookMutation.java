package ru.lib.graphql.graphql.mutations;

import javassist.NotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import ru.lib.graphql.domain.Book;
import ru.lib.graphql.services.BookService;
import ru.lib.graphql.services.errors.ObjectNotFoundException;
import ru.lib.graphql.services.pubs.BookCreatedEventPublisher;
import ru.lib.graphql.services.requests.BookCreateRequest;
import ru.lib.graphql.services.requests.BookEditRequest;

import javax.validation.Valid;

@Component
@Validated
public class BookMutation extends Mutation {
  private final BookService bookService;
  private final BookCreatedEventPublisher publisher;

  public BookMutation(BookService bookService, BookCreatedEventPublisher publisher) {
    this.bookService = bookService;
    this.publisher = publisher;
  }

  public Book createBook(@Valid BookCreateRequest request){
    Book book =  bookService.createBook(request);
    publisher.publish(book);
    return book;
  }

  public Book updateBook(BookEditRequest request) throws ObjectNotFoundException {
    return bookService.updateBook(request);
  }

  public Boolean deleteBook(int id){
    return bookService.deleteBook(id);
  }
}
