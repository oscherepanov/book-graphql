package ru.lib.graphql.graphql.mutations;

import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.SignData;
import ru.lib.graphql.services.SignInService;
import ru.lib.graphql.services.requests.AuthData;

@Component
public class SignInMutation extends Mutation {

  private final SignInService signInService;

  public SignInMutation(SignInService signInService) {
    this.signInService = signInService;
  }

  public SignData signIn(AuthData auth){
    return signInService.signIn(auth);
  }

}
