package ru.lib.graphql.graphql.mutations;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.Journal;
import ru.lib.graphql.services.JournalService;
import ru.lib.graphql.services.requests.JournalCreateRequest;
import ru.lib.graphql.services.requests.JournalEditRequest;

@Component
public class JournalMutation extends Mutation {
  @Autowired
  private JournalService journalService;

  public Journal createJournal(final JournalCreateRequest request) throws NotFoundException {
    return journalService.createJournal(request);
  }

  public Journal updateJournal(final JournalEditRequest request) throws NotFoundException {
    return journalService.updateJournal(request);
  }

  public Boolean deleteJournal(Integer id) {
    return journalService.deleteJournal(id);
  }
}
