package ru.lib.graphql.graphql.mutations;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.Reader;
import ru.lib.graphql.services.ReaderService;
import ru.lib.graphql.services.requests.ReaderCreateRequest;
import ru.lib.graphql.services.requests.ReaderEditRequest;

@Component
public class ReaderMutation extends Mutation {
  @Autowired
  private ReaderService readerService;

  public Reader createReader(ReaderCreateRequest request) {
    return readerService.createReader(request);
  }

  public Reader updateReader(ReaderEditRequest request) throws NotFoundException {
    return readerService.updateReader(request);
  }

  public Boolean deleteReader(Integer id) {
    return readerService.deleteReader(id);
  }

}
