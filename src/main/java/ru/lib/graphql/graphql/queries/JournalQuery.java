package ru.lib.graphql.graphql.queries;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.*;
import ru.lib.graphql.services.*;

import java.util.List;

@Component
public class JournalQuery extends Query {
  @Autowired
  private JournalService journalService;

  public List<Journal> getJournal(){
    return this.journalService.getAll();
  }

  public Journal findJournalRecord(Integer id) throws NotFoundException {
    return journalService.findOne(id);
  }
}
