package ru.lib.graphql.graphql.queries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.lib.graphql.domain.Book;
import ru.lib.graphql.services.BookService;

import java.util.List;

@Component
public class BookQuery extends Query {
  @Autowired
  private BookService bookService;

  public List<Book> allBooks(){
    return this.bookService.getAllBooks();
  }

}
