package ru.lib.graphql.graphql.queries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.Employee;
import ru.lib.graphql.services.EmployeeService;

import java.util.List;

@Component
public class EmployeeQuery extends Query {
  @Autowired
  private EmployeeService employeeService;

  public List<Employee> allEmployees(){
    return this.employeeService.getAllEmployees();
  }

}
