package ru.lib.graphql.graphql.queries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.Author;
import ru.lib.graphql.services.AuthorService;

import java.util.List;

@Component
public class AuthorQuery extends Query {
  @Autowired
  private AuthorService authorService;

  public List<Author> allAuthors(){
    return this.authorService.getAllAuthors();
  }

}
