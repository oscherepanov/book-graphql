package ru.lib.graphql.graphql.queries;

import graphql.schema.DataFetchingEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.Genre;
import ru.lib.graphql.graphql.annotation.Signed;
import ru.lib.graphql.services.GenreService;

import java.util.List;

@Component
public class GenreQuery extends Query {
  @Autowired
  private GenreService genreService;

  @Signed
  public List<Genre> allGenres(DataFetchingEnvironment env) {
    return this.genreService.getAllGenre();
  }

}
