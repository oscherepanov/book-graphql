package ru.lib.graphql.graphql.queries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.Reader;
import ru.lib.graphql.services.ReaderService;

import java.util.List;

@Component
public class ReaderQuery extends Query {
  @Autowired
  private ReaderService readerService;

  public List<Reader> allReaders(){
    return this.readerService.getAllReaders();
  }
}
