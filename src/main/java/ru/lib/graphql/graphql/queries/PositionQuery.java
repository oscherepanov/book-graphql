package ru.lib.graphql.graphql.queries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.Position;
import ru.lib.graphql.services.PositionService;

import java.util.List;

@Component
public class PositionQuery extends Query {
  private final PositionService positionService;

  public PositionQuery(PositionService positionService) {
    this.positionService = positionService;
  }

  public List<Position> allPositions() {
    return this.positionService.getAllPositions();
  }

}
