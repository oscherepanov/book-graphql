package ru.lib.graphql.graphql.subs;

import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.Book;
import ru.lib.graphql.services.pubs.BookCreatedEventPublisher;

@Component
public class BookSubscription extends Subscription {

  @Autowired
  BookCreatedEventPublisher publisher;

  public Publisher<Book> newBook(){
    return publisher.getPublisher();
  }
}
