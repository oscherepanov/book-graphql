package ru.lib.graphql.services;

import ru.lib.graphql.domain.Genre;
import org.springframework.stereotype.Service;
import ru.lib.graphql.repositories.GenreRepository;

import java.util.List;

@Service
public class GenreService {
  private final GenreRepository repository;

  public GenreService(final GenreRepository repository){
    this.repository = repository;
  }

  public List<Genre> getAllGenre(){
    return repository.findAll();
  }
}
