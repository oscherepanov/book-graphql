package ru.lib.graphql.services;

import javassist.NotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.lib.graphql.domain.Author;
import ru.lib.graphql.services.requests.AuthorCreateRequest;
import ru.lib.graphql.repositories.AuthorRepository;
import ru.lib.graphql.services.requests.AuthorEditRequest;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorService {
  private final AuthorRepository repository;

  public AuthorService(final AuthorRepository repository){
    this.repository = repository;
  }

  public List<Author> getAllAuthors(){
    return repository.findAll();
  }

  public Author createAuthor(final AuthorCreateRequest request){
    Author author = new Author();
    author.setName(request.getName());
    return repository.save(author);
  }

  public Author updateAuthor(final AuthorEditRequest request) throws NotFoundException {
    Optional<Author> optionalReader = repository.findById(request.getId());
    if (optionalReader.isPresent()) {
      Author author = optionalReader.get();
      author.setName(request.getName());
      return repository.save(author);
    } else
      throw new NotFoundException("Author not found");
  }

  public Boolean deleteAuthor(int id){
    repository.deleteById(id);
    return true;
  }
}
