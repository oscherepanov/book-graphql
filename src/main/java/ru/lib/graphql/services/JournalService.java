package ru.lib.graphql.services;

import javassist.NotFoundException;
import org.springframework.stereotype.Service;
import ru.lib.graphql.domain.Book;
import ru.lib.graphql.domain.Employee;
import ru.lib.graphql.domain.Journal;
import ru.lib.graphql.domain.Reader;
import ru.lib.graphql.repositories.BookRepository;
import ru.lib.graphql.repositories.EmployeeRepository;
import ru.lib.graphql.repositories.JournalRepository;
import ru.lib.graphql.repositories.ReaderRepository;
import ru.lib.graphql.services.requests.JournalCreateRequest;
import ru.lib.graphql.services.requests.JournalEditRequest;

import java.util.List;
import java.util.Optional;

@Service
public class JournalService {
  private final JournalRepository journalRepository;
  private final BookRepository bookRepository;
  private final ReaderRepository readerRepository;
  private final EmployeeRepository employeeRepository;

  public JournalService(JournalRepository journalRepository, BookRepository bookRepository,
                        ReaderRepository readerRepository, EmployeeRepository employeeRepository) {
    this.journalRepository = journalRepository;
    this.bookRepository = bookRepository;
    this.readerRepository = readerRepository;
    this.employeeRepository = employeeRepository;
  }

  public List<Journal> getAll(){
    return journalRepository.findAll();
  }

  public Journal findOne(Integer id) throws NotFoundException {
    Optional<Journal> optionalJournal = journalRepository.findById(id);
    if (optionalJournal.isPresent())
      return optionalJournal.get();
    throw new NotFoundException("Journal not founded");
  }

  public Journal createJournal(final JournalCreateRequest request) throws NotFoundException {
    Optional<Book> optionalBook = bookRepository.findById(request.getBook_id()) ;
    if (!optionalBook.isPresent())
      throw new NotFoundException("Book is not found");

    Optional<Employee> optionalEmployee = employeeRepository.findById(request.getEmployee_id());
    if (!optionalEmployee.isPresent())
      throw new NotFoundException("Employee is not found");

    Optional<Reader> optionalReader = readerRepository.findById(request.getReader_id());
    if (!optionalReader.isPresent())
      throw new NotFoundException("Reader is not found");

    Journal journal = new Journal();
    journal.setAction(request.getAction());
    journal.setBook(optionalBook.get());
    journal.setReader(optionalReader.get());
    journal.setEmployee(optionalEmployee.get());
    journal.setCreated(request.getCreated());
    return journalRepository.save(journal);
  }

  public Journal updateJournal(final JournalEditRequest request) throws NotFoundException {

    Optional<Journal> optionalJournal = journalRepository.findById(request.getId());
    if (!optionalJournal.isPresent())
      throw new NotFoundException("Journal is not found");

    Optional<Book> optionalBook = bookRepository.findById(request.getBook_id()) ;
    if (!optionalBook.isPresent())
      throw new NotFoundException("Book is not found");

    Optional<Employee> optionalEmployee = employeeRepository.findById(request.getEmployee_id());
    if (!optionalEmployee.isPresent())
      throw new NotFoundException("Employee is not found");

    Optional<Reader> optionalReader = readerRepository.findById(request.getReader_id());
    if (!optionalReader.isPresent())
      throw new NotFoundException("Reader is not found");

    Journal journal = new Journal();
    journal.setId(request.getId());
    journal.setAction(request.getAction());
    journal.setBook(optionalBook.get());
    journal.setReader(optionalReader.get());
    journal.setEmployee(optionalEmployee.get());
    journal.setCreated(request.getCreated());
    return journalRepository.save(journal);
  }

  public Boolean deleteJournal(int id){
    journalRepository.deleteById(id);
    return true;
  }

}
