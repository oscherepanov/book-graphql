package ru.lib.graphql.services;

import graphql.GraphQLException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.lib.graphql.domain.SignData;
import ru.lib.graphql.domain.User;
import ru.lib.graphql.repositories.UserRepository;
import ru.lib.graphql.services.requests.AuthData;

import java.util.UUID;

@Service
public class SignInService {
  private final UserRepository userRepository;

  public SignInService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Transactional
  public SignData signIn(final AuthData data) {
    User user = userRepository.findByEmail(data.getEmail());
    if (user != null && user.getPassword().equals(data.getPassword())){
      String token = generateToken();
      user.setToken(token);
      userRepository.save(user);
      return new SignData(token);
    }
    throw new GraphQLException("Invalid credentials");
  }

  private String generateToken(){
    UUID uuid = UUID.randomUUID();
    return uuid.toString();
  }
}
