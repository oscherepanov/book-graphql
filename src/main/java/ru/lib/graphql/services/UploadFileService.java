package ru.lib.graphql.services;

import org.aarboard.nextcloud.api.NextcloudConnector;
import org.aarboard.nextcloud.api.filesharing.Share;
import org.aarboard.nextcloud.api.filesharing.SharePermissions;
import org.aarboard.nextcloud.api.filesharing.ShareType;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Component
public class UploadFileService {

  public String uploadFile(String filename, List<InputStream> parts){
    try {
      Path path = concatFiles(filename, parts);
      return uploadToNextcloud(path.toFile(), filename);
    }
    catch (IOException e){
      // TODO exception
    }
    return ""; //TODO remove
  }

  private Path concatFiles(String filename, List<InputStream> parts) throws IOException {
    Path tmp_dir = Files.createTempDirectory("");
    File file = new File(Paths.get(tmp_dir.toString(), filename).toString());
    FileOutputStream out = new FileOutputStream(file);

    for(InputStream stream: parts){
      byte[] buf = new byte[8192];
      int len;
      while ((len = stream.read(buf)) > 0) {
        out.write(buf, 0, len);
      }
    }
    out.close();
    return file.toPath();
  }

  private String uploadToNextcloud(File file, String filename){
    NextcloudConnector connector = new NextcloudConnector("nextcloud.poas45.ru", true,
      443, "test", "PtktystHtkmcs");
    String path = "shared/" + file.getName();
    connector.uploadFile(file, path);
    Share share = connector.doShare(path, ShareType.PUBLIC_LINK, "test", false,
      "", new SharePermissions(1));
    return share.getUrl()+"/download";
  }
}
