package ru.lib.graphql.services;

import org.springframework.stereotype.Service;
import ru.lib.graphql.domain.User;
import ru.lib.graphql.repositories.UserRepository;
import ru.lib.graphql.services.requests.AuthData;

@Service
public class UserService {
  private final UserRepository userRepository;

  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public User createUser(final String name, final AuthData data){
    User user = new User();
    user.setName(name);
    user.setEmail(data.getEmail());
    user.setPassword(data.getPassword());
    return userRepository.save(user);
  }
}
