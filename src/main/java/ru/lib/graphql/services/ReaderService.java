package ru.lib.graphql.services;

import javassist.NotFoundException;
import org.springframework.stereotype.Service;
import ru.lib.graphql.domain.Reader;
import ru.lib.graphql.services.requests.ReaderCreateRequest;
import ru.lib.graphql.services.requests.ReaderEditRequest;
import ru.lib.graphql.repositories.ReaderRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ReaderService {
  private final ReaderRepository repository;

  public ReaderService(final ReaderRepository repository){
    this.repository = repository;
  }

  public List<Reader> getAllReaders(){
    return repository.findAll();
  }

  public Reader createReader(final ReaderCreateRequest request){
    Reader reader = new Reader();
    reader.setName(request.getName());
    return repository.save(reader);
  }

  public Reader updateReader(final ReaderEditRequest request) throws NotFoundException {
    Optional<Reader> optionalReader = repository.findById(request.getId());
    if (optionalReader.isPresent()) {
      Reader reader = optionalReader.get();
      reader.setName(request.getName());
      return repository.save(reader);
    } else
      throw new NotFoundException("Reader not found");
  }

  public Boolean deleteReader(Integer id) {
    repository.deleteById(id);
    return true;
  }
}
