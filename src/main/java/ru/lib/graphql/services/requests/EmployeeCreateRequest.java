package ru.lib.graphql.services.requests;

import lombok.Data;

@Data
public class EmployeeCreateRequest {
  private String name;
  private Integer position_id;
}
