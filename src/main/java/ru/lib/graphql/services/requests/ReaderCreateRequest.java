package ru.lib.graphql.services.requests;

import lombok.Data;

@Data
public class ReaderCreateRequest {
  private String name;
}
