package ru.lib.graphql.services.requests;

import lombok.Data;

@Data
public class ReaderEditRequest {
  private Integer id;
  private String name;
}
