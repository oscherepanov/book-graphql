package ru.lib.graphql.services.requests;

import lombok.Data;

@Data
public class AuthorEditRequest {
  private Integer id;
  private String name;
}
