package ru.lib.graphql.services.requests;

import lombok.Data;

import javax.validation.constraints.*;
import java.util.List;

@Data
public class BookCreateRequest {

  @NotNull
  @Size(min = 3, max = 50)
  private String name;

  @Min(value = 1000)
  @Max(value = 2000)
  private Integer year;

  @Min(value = 1)
  private Integer genre_id;

  @NotEmpty(message = "У книги должны быть указаны авторы")
  List<Integer> author_ids;
}
