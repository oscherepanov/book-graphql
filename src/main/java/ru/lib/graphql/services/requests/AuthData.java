package ru.lib.graphql.services.requests;

import lombok.Data;

@Data
public class AuthData {
  private String email;
  private String password;
}
