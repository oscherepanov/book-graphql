package ru.lib.graphql.services.requests;

import lombok.Data;

@Data
public class AuthorCreateRequest {
  private String name;
}
