package ru.lib.graphql.services.requests;

import lombok.Data;
import ru.lib.graphql.domain.Action;

@Data
public class JournalEditRequest {
  private Integer id;
  private Integer book_id;
  private Integer reader_id;
  private Integer employee_id;
  private Action action;
  private String created;
}
