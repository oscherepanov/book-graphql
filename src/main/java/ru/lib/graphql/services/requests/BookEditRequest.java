package ru.lib.graphql.services.requests;

import lombok.Data;

import java.util.List;

@Data
public class BookEditRequest {
  private Integer id;
  private String name;
  private Integer year;
  private Integer genre_id;
  private List<Integer> author_ids;
}

