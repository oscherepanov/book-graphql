package ru.lib.graphql.services.requests;

import lombok.Data;

@Data
public class EmployeeEditRequest {
  private Integer id;
  private String name;
  private Integer position_id;
}
