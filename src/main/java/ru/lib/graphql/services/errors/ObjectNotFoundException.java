package ru.lib.graphql.services.errors;

import graphql.ErrorType;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.List;

public class ObjectNotFoundException extends RuntimeException implements GraphQLError {

  public ObjectNotFoundException(String message) {
    super(message, null, true, false);
  }

  @Override
  public List<SourceLocation> getLocations() {
    return null;
  }

  @Override
  public ErrorType getErrorType() {
    return ErrorType.DataFetchingException;
  }
}
