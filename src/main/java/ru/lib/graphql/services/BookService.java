package ru.lib.graphql.services;

import javassist.NotFoundException;
import org.springframework.stereotype.Service;
import ru.lib.graphql.domain.Author;
import ru.lib.graphql.domain.Book;
import ru.lib.graphql.domain.Genre;
import ru.lib.graphql.repositories.AuthorRepository;
import ru.lib.graphql.repositories.BookRepository;
import ru.lib.graphql.repositories.GenreRepository;
import ru.lib.graphql.services.errors.ObjectNotFoundException;
import ru.lib.graphql.services.requests.BookCreateRequest;
import ru.lib.graphql.services.requests.BookEditRequest;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class BookService {
  private final BookRepository bookRepository;
  private final AuthorRepository authorRepository;
  private final GenreRepository genreRepository;

  public BookService(final BookRepository bookRepository,
                     final AuthorRepository authorRepository,
                     final GenreRepository genreRepository){
    this.bookRepository = bookRepository;
    this.authorRepository = authorRepository;
    this.genreRepository = genreRepository;
  }

  @Transactional
  public List<Book> getAllBooks(){
    return bookRepository.findAll();
  }

  public Book createBook(final BookCreateRequest request) {
    List<Author> authors = authorRepository.findByIds(request.getAuthor_ids());
    if (authors.isEmpty())
      throw new ObjectNotFoundException("Authors not found");
    Optional<Genre> optionalGenre = genreRepository.findById(request.getGenre_id());
    if (optionalGenre.isPresent()){
      Genre genre = optionalGenre.get();
      Book book = new Book();
      book.setName(request.getName());
      book.setGenre(genre);
      book.setYear(request.getYear());
      book.setAuthors(new HashSet<>(authors));
      return bookRepository.save(book);
    }
    else
      throw new ObjectNotFoundException("Genre not found");
  }

  public Book updateBook(final BookEditRequest request) throws ObjectNotFoundException {
    Optional<Book> optionalBook = bookRepository.findById(request.getId());
    if (optionalBook.isPresent()){
      Book book = optionalBook.get();
      List<Author> authors = authorRepository.findByIds(request.getAuthor_ids());
      if (authors.isEmpty())
        throw new ObjectNotFoundException("Authors not found");
      Optional<Genre> optionalGenre = genreRepository.findById(request.getGenre_id());
      if (optionalGenre.isPresent()) {
        Genre genre = optionalGenre.get();
        book.setName(request.getName());
        book.setGenre(genre);
        book.setYear(request.getYear());
        book.setAuthors(new HashSet<Author>(authors));
        return bookRepository.save(book);
      } else
        throw new ObjectNotFoundException("Genre not found");
    }
    else throw new ObjectNotFoundException("Book not found");
  }

  public Boolean deleteBook(int id){
    bookRepository.deleteById(id);
    return true;
  }
}
