package ru.lib.graphql.services.pubs;

import io.reactivex.*;
import io.reactivex.observables.ConnectableObservable;
import org.springframework.stereotype.Component;
import ru.lib.graphql.domain.Book;

@Component
public class BookCreatedEventPublisher {
  private Flowable<Book> publisher;
  private ObservableEmitter<Book> emitter;

  public BookCreatedEventPublisher() {
    Observable<Book> bookUpdateObservable = Observable.create(new ObservableOnSubscribe<Book>() {
                                                                @Override
                                                                public void subscribe(ObservableEmitter<Book> observableEmitter) throws Exception {
                                                                  emitter = observableEmitter;
                                                                }
                                                              });

    ConnectableObservable <Book> connectableObservable = bookUpdateObservable.publish();
    connectableObservable.connect();

    publisher = connectableObservable.toFlowable(BackpressureStrategy.BUFFER);
  }

  public void publish(final Book book) {
    emitter.onNext(book);
  }

  public Flowable<Book> getPublisher() {
    return this.publisher;
  }
}
