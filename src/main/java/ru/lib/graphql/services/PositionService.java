package ru.lib.graphql.services;

import org.springframework.stereotype.Service;
import ru.lib.graphql.domain.Genre;
import ru.lib.graphql.domain.Position;
import ru.lib.graphql.repositories.GenreRepository;
import ru.lib.graphql.repositories.PositionRepository;

import java.util.List;

@Service
public class PositionService {
  private final PositionRepository repository;

  public PositionService(final PositionRepository repository){
    this.repository = repository;
  }

  public List<Position> getAllPositions(){
    return repository.findAll();
  }
}
