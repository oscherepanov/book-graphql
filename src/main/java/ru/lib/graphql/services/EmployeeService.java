package ru.lib.graphql.services;

import javassist.NotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.lib.graphql.domain.Employee;
import ru.lib.graphql.domain.Position;
import ru.lib.graphql.services.requests.EmployeeCreateRequest;
import ru.lib.graphql.repositories.EmployeeRepository;
import ru.lib.graphql.repositories.PositionRepository;
import ru.lib.graphql.services.requests.EmployeeEditRequest;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
  private final EmployeeRepository employeeRepository;
  private final PositionRepository positionRepository;

  public EmployeeService(final EmployeeRepository employeeRepository,
                         final PositionRepository positionRepository){
    this.employeeRepository = employeeRepository;
    this.positionRepository = positionRepository;
  }

  public List<Employee> getAllEmployees(){
    return employeeRepository.findAll();
  }

  public Employee createEmployee(EmployeeCreateRequest request) throws NotFoundException {
    Employee emp = new Employee();
    emp.setName(request.getName());
    Optional<Position> position = positionRepository.findById(request.getPosition_id());
    if (position.isPresent()) {
      emp.setPosition(position.get());
      return employeeRepository.save(emp);
    }
    else
      throw new NotFoundException("Employee not found");
  }

  public Employee updateEmployee(final EmployeeEditRequest request) throws NotFoundException {
    Optional<Employee> optionalEmployee = employeeRepository.findById(request.getId());
    if (optionalEmployee.isPresent()) {
      Employee employee = optionalEmployee.get();
      employee.setName(request.getName());

      Optional<Position> position = positionRepository.findById(request.getPosition_id());
      if (position.isPresent()) {
        employee.setPosition(position.get());
        return employeeRepository.save(employee);
      }
      else
        throw new NotFoundException("Employee not found");
    } else
      throw new NotFoundException("Reader not found");
  }

  public Boolean deleteEmployee(Integer id) {
    employeeRepository.deleteById(id);
    return true;
  }
}
